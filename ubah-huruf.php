<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ubah_huruf</title>
</head>
<body>
    <?php
function ubah_huruf($string){
    //kode di sini
    $huruf = "abcdefghijklmnopqrstuvwxyz";
    $new_string = "";
    for ($i=0; $i < strlen($string); $i++) { 
        for ($j=0; $j < strlen($huruf); $j++) { 
            if ($string[$i] == $huruf[$j]) {
                $new_string .= $huruf[$j+1];
            }
        }
    }
    return $new_string;
}
    
// TEST CASES
echo ubah_huruf('wow')."<br>"; // xpx
echo ubah_huruf('developer')."<br>"; // efwfmpqfs
echo ubah_huruf('laravel')."<br>"; // mbsbwfm
echo ubah_huruf('keren')."<br>"; // lfsfo
echo ubah_huruf('semangat')."<br>"; // tfnbohbu

?>
</body>
</html>