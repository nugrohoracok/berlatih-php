<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        function tukar_besar_kecil($string)
        {
            //kode di sini;
            $new_string = "";
            $lowcase = "qwertyuiopasdfghjklzxcvbnm";
            $upcase = strtoupper($lowcase);
            $char = "0123456789-+/* ";
            for ($j=0; $j < strlen($string); $j++) {
                for ($i=0; $i < strlen($char); $i++) { 
                    if ($string[$j] == $char[$i]) {
                        $new_string .= $string[$j];
                    }
                }
                for ($i=0; $i < strlen($lowcase); $i++) { 
                    if ($string[$j] == $lowcase[$i]) {
                        $new_string .= strtoupper($string[$j]);
                    }
                    if ($string[$j] == $upcase[$i]) {
                        $new_string .= strtolower($string[$j]);
                    }
                }
            }
        return $new_string;
        }

        // TEST CASES
        echo tukar_besar_kecil('Hello World')."<br>"; // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY')."<br>"; // "i Am Alay"
        echo tukar_besar_kecil('My Name is Bond!!')."<br>"; // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me')."<br>"; // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW')."<br>"; // "001-a-3-5tRDyw"
        echo ("a"=="b");

    ?>
</body>
</html>